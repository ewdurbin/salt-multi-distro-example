# salt-multi-distro-example

Simple salt setup for installing apache web server and disabling root login

## Setup 

1. Download and Install [Virtual Box](https://www.virtualbox.org/wiki/Downloads).

2. Download and Install [Vagrant](http://downloads.vagrantup.com/)

3. `vagrant plugin install vagrant-salt`

4. Choose a box type (CentOS 6 or Ubuntu 12.04) `cp Vagrantfile.{centos,ubuntu} Vagrantfile`

5. `vagrant up`

6. Grab some coffee? Downloading the box and initial bootstrap may take a while

## Note

`vagrant up` and `vagrant provision` are very noisy because of the Vagrantfile configurations. if you prefer you can disable the s.verbose option, and perform a `vagrant ssh` followed by a `sudo salt-call state.highstate`.
