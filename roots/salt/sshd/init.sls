/etc/ssh/sshd_config:
  file.comment:
    - regex: "^PermitRootLogin yes"

disable-root:
  file.append:
    - name: /etc/ssh/sshd_config
    - text: "PermitRootLogin no"

ssh-daemon:
  service:
    - name: {{ pillar['sshd'] }}
    - running
    - enable: True
    - watch:
      - file: /etc/ssh/sshd_config
    
